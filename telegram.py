############################################
# Python 3 Telegram Bot API connector v1.1 #
# Copyright (C) Pietu1998 2015             #
############################################

################################################################################
# DON'T BE A DICK PUBLIC LICENSE TERMS AND CONDITIONS FOR COPYING,             #
# DISTRIBUTION AND MODIFICATION                                                #
#                                                                              #
# 1. Do whatever you like with the original work, just don't be a dick.        #
#                                                                              #
#    Being a dick includes - but is not limited to - the following instances:  #
#                                                                              #
#       1a. Outright copyright infringement - Don't just copy this and change  #
#           the name.                                                          #
#       1b. Selling the unmodified original with no work done what-so-ever,    #
#           that's REALLY being a dick.                                        #
#       1c. Modifying the original work to contain hidden harmful content.     #
#           That would make you a PROPER dick.                                 #
#                                                                              #
# 2. If you become rich through modifications, related works/services, or      #
#    supporting the original work, share the love. Only a dick would make      #
#    loads off this work and not buy the original work's creator(s) a pint.    #
#                                                                              #
# 3. Code is provided with no warranty. Using somebody else's code and         #
#    bitching when it goes wrong makes you a DONKEY dick. Fix the problem      #
#    yourself. A non-dick would submit the fix back.                           #
################################################################################


class Bot:
    def __init__(self, token, offset = 0):
        self.api = "https://api.telegram.org/%sbot%s/"
        self.token = token
        self.offset = offset
        self._my_info = None
    def confirmToken(self):
        result = self.makeRequest("getMe")
        if "result" in result:
            self._my_info = result["result"]
    def getBotId(self):
        if self._my_info is None:
            self.confirmToken()
        return self._my_info["id"]
    def getBotUser(self):
        if self._my_info is None:
            self.confirmToken()
        return self._my_info["username"]
    def clearUpdates(self):
        emptyBuffer = self.getUpdates(0)
        while len(emptyBuffer) > 0:
            self.offset = emptyBuffer[-1]["update_id"]
            emptyBuffer = self.getUpdates(0)
    def getUpdates(self, timeout = 60):
        data = {"offset": self.offset + 1}
        if timeout > 0:
            data["timeout"] = timeout
        updates = self.makeRequest("getUpdates", data)
        if len(updates["result"]) > 0:
            self.offset = updates["result"][-1]["update_id"]
        return updates["result"]
    def getFile(self, path):
        return self.makeRequest(path, raw = True, prefix = "file/")
    def doRequest(self, method, data = None):
        result = self.makeRequest(method, data)
        return result["result"]
    def _handleResult(self, result, opts):
        from json import loads
        if not opts.get("raw", False):
            result = loads(result.read().decode("utf-8"))
            if "ok" not in result.keys():
                raise ApiError("bad response")
            elif not result["ok"]:
                raise ApiError(result["description"])
            else:
                return result
        else:
            return result.read()
    def makeRequest(self, method, data = None, **opts):
        from urllib.request import urlopen, Request
        from urllib.error import HTTPError
        from json import dumps
        apiurl = self.api % (opts.get("prefix", ""), self.token) + method
        try:
            timeout = 15
            if data is not None:
                if "timeout" in data.keys() and (isinstance(data["timeout"], int) or data["timeout"].isdigit()):
                    timeout += int(data["timeout"])
                data = bytes(dumps(data), "utf-8")
                request = Request(apiurl, data, {"Content-Type": "application/json"})
            else:
                request = Request(apiurl)
            response = urlopen(request, timeout = timeout)
            return self._handleResult(response, opts)
        except KeyboardInterrupt:
            raise KeyboardInterrupt()
        except HTTPError as e:
            try:
                return self._handleResult(e, opts)
            except Exception as e2:
                raise ApiError("loading failed: " + str(e) + ", " +  str(e2))
        except Exception as e:
            raise ApiError("loading failed: " + str(e))

class ApiError(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message
